# 변수는 값을 저장하는 저장공간으로 여러 타입을 갖습니다.

'''
따옴표 세개를 연속으로 쓰면 여러 줄에 걸쳐서 주석을 달 수 있습니다.
함수에 따옴표 세개의 주석을 쓰면 docstring으로 사용할 수 있습니다.
따옴표 1개 ' 와 2개 " 는 일반적인 문자열을 만드는 데 사용됩니다.
'''

"""
변수명 만드는 규칙:
  - 변수명은 대소문자를 구분합니다(name과 NAME은 서로 다른 변수명입니다).
  - 변수명은 반드시 문자(a-z/A-Z), 숫자(0-9), 또는 언더스코어(_)로 시작해야합니다.
  - 숫자는 변수명의 처음에는 올 수 없습니다.
"""

# x = 1
# y = 2.5
# name = 'John'
# is_cool = True

x, y, name, is_cool = (1, 2.5, 'John', True)

# print(type(x), x)
# print(type(y), y)
# print(type(name), name)
# print(type(is_cool), is_cool)

a = x + y

x = str(x)
y = int(y)
#name = float(name) # ERROR!

#print(type(x), x)
#print(type(y), y)
#print(type(name), name)

