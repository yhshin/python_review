# If/ Else 조건은 참, 거짓에 따라 다른 실행을 하도록 할 때 사용한다.

## 비교연산자(==, !=, >, <, >=, <=) - 값 비교에 사용
### Simple if
### If/else
### elif
### Nested if

## 논리연산자(and, or, not) - 조건문을 연결할 때 사용
### and
### or
### not

## 멤버십 연산자(in, not in) - 시퀀스에 해당 항목이 있는지 테스트
###  in
### not in

## 동일연산자(is, is not) - 동일 메모리 테스트
### is
### is not
