# 함수는 호출하여 실행하는 코드의 묶음이다.
# 묶음을 표시하기 위해 들여쓰기(보통 4칸)를 이용한다.


## 함수 만들기
def sayHello(name='Sam'):
    print(f'Hello {name}')

sayHello()
sayHello("Tom")

## 함수 리턴
def getSum(num1, num2):
    total = num1 + num2
    return total

print(getSum(1,2))

# 람다함수는 이름없는 함수이다.
getSum_lambda = lambda num1, num2: num1 + num2

print(getSum_lambda(10, 3))
