# 파이썬에서 문자열은 따옴표 1개 또는 2개로 만든다.
# 문자열의 포맷과 메소드를 살펴보자.

name = 'Brad'
age = 37

# 문자열 이어붙이기
print('Hello, my name is ' + name + ' and I am ' + str(age))

# 문자열 포맷

## format 함수 사용
print('My name is {name} and I am {age}'.format(name=name, age=age))

## F-문자열 사용 (3.6+)
print(f'Hello, my name is {name} and I am {age}')

# 문자열 메소드

s = 'helloworld'

## 문자열의 첫 문자를 대문자로 변경하기
print(s.capitalize())

## 문자열 전체를 대문자로 변경하기
print(s.upper())

## 문자열 전체를 소문자로 변경하기
print(s.lower())

## 문자열 전체의 대소문자 서로 바꿔치기
print(s.swapcase())

## 문자열 크기
print(len(s))

## 문자열의 첫번째 인수에 해당하는 부분을 두번째 인수로 교체
print(s.replace('world', 'everyone'))

## 해당 문자열 갯수 세기
sub = 'h'
print(s.count(sub))

## 문자열의 처음이 해당 문자열로 시작하는지 참, 거짓 반환
print(s.startswith('hello'))

## 문자열의 끝이 해당 문자열로 시작하는지 참, 거짓 반환
print(s.endswith('d'))

## 문자열을 디폴트로 빈문자로 구분하고 리스트로 반환 
print(s.split())

## 문자열에 해당 문자열의 위치 반환
print(s.find('r'))

## 문자열이 모두 문자나 숫자인지 참, 거짓 반환 
print(s.isalnum())

## 문자열이 모두 문자인지 참, 거짓 반환 
print(s.isalpha())

## 문자열이 모두 숫자인지 참, 거짓 반환 
print(s.isnumeric())
