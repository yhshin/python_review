# class는 물건(object)을 찍어내는 거푸집과 같다.
# object(객체)는 properties와 methods를 갖는다.
# 파이썬의 모든 것은 객체이다.

## class 만들기
class User:
  # Constructor
  def __init__(self, name, email, age):
    self.name = name
    self.email = email
    self.age = age

  def greeting(self):
      return f'My name is {self.name} and I am {self.age}'

  def has_birthday(self):
      self.age += 1
 
## class 상속(Customer)
class Customer(User):
  # Constructor
  def __init__(self, name, email, age):
      User.__init__(self, name, email, age) #Called proper parent class constructor to make this as proper child inehriting all methods.
      self.balance = 0

  def set_balance(self, balance):
      self.balance = balance

  def greeting(self):
      return f'My name is {self.name} and I am {self.age} and my balance is {self.balance}'

## Init user object
brad = User('Brad Traversy', 'brad@gmail.com', 37)
## Init customer object
janet = Customer('Janet Johnson', 'janet@yahoo.com', 25)
janet.set_balance(500)

print(brad.greeting())
brad.has_birthday()
print(brad.greeting())
brad.has_birthday()
print(brad.greeting())

print(janet.greeting())
janet.has_birthday()
print(janet.greeting())
janet.has_birthday()
print(janet.greeting())