# 리스트는 변경가능하고 순서가 있는 데이터의 묶음이다.
# 리스트에 동일한 값이 있을 수 있다.

## 리스트 만들기
numbers = [1, 2, 3, 4, 5]
fruits = ['Apples', 'Oranges', 'Grapes', 'Pears']
numbers2 = list((1, 2, 3, 4, 5))

## 리스트에서 하나의 항목 가져오기
print(fruits[1])

## 리스트에서 마지막 항목 가져오기
print(fruits[-1])

## 리스트의 항목 갯수
print(len(fruits))

## 리스트 맨 뒤에 항목 추가
fruits.append('Mangos')

## 리스트의 항목 삭제
fruits.remove('Grapes')

## 리스트 원하는 위치에 항목 추가
fruits.insert(2, 'Strawberries')

## 리스트 항목 내용 변경
fruits[0] = 'Blueberries'

## pop 메서드
fruits.pop(2)

## 리스트 순서 거꾸로 바꾸기
fruits.reverse()

## sort 메서드
fruits.sort()

## Reverse sort 메서드
fruits.sort(reverse=True)
