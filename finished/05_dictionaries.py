# 딕셔너리는 순서가 없고, 변경가능하며, 인덱싱이 되어 있는 데이터의 묶음이다.
# 동일한 항목은 넣을 수 없다.

## 딕셔너리 만들기
person = {
    'first_name': 'John',
    'last_name': 'Doe',
    'age': 30
}

person2 = dict(first_name='Sara', last_name='Williams')

## Value 가져오기
print(person['first_name'])
print(person.get('last_name'))

## key/value 추가하기
person['phone'] = '555-555-5555'

## 딕셔너리의 전체 key들의 리스트
print(person.keys())

## 딕셔너리의 전체 key/value를 튜플로 하는 리스트
print(person.items())

## 딕셔너리 복사
person2 = person.copy()
person2['city'] = 'Boston'

## 항목 삭제
del(person['age'])
person.pop('phone')

## 내용 지우기
person.clear()

## 딕셔너리의 항목 숫자
print(len(person2))

## 딕셔너리의 리스트
people = [
    {'name': 'Martha', 'age': 30},
    {'name': 'Kevin', 'age': 25}
]
