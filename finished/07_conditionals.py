# If/ Else 조건은 참, 거짓에 따라 다른 실행을 하도록 할 때 사용한다.

x = 21
y = 20
## 비교연산자(==, !=, >, <, >=, <=) - 값 비교에 사용
### Simple if
if x > y:
    print(f'{x} is greater than {y}')

x = 20
y = 21
### If/else
if x > y:
    print(f'{x} is greater than {y}')
else:
    print(f'{y} is greater than {x}')  

x = 20
y = 20
### elif
if x > y:
    print(f'{x} is greater than {y}')
elif x == y:
    print(f'{x} is equal to {y}')  
else:
    print(f'{y} is greater than {x}')  

x = 8
### Nested if
if x > 2:
    if x <= 10:
        print(f'{x} is greater than 2 and less than or equal to 10')
    
x = 8
## 논리연산자(and, or, not) - 조건문을 연결할 때 사용
### and
if x > 2 and x <= 10:
    print(f'{x} is greater than 2 and less than or equal to 10')

x = 11
### or
if x < 2 or x >= 10:
    print(f'{x} is greater than 2 or less than or equal to 10')

x = 10
y = 11
### not
if not(x == y):
    print(f'{x} is not equal to {y}')

if x != y:
    print(f'{x} is not equal to {y}')

## 멤버십 연산자(in, not in) - 시퀀스에 해당 항목이 있는지 테스트

numbers = [1,2,3,4,5]

x = 3
###  in
if x in numbers:
    print(x in numbers)

x = 7
### not in
if x not in numbers:
  print(x not in numbers)

## 동일연산자(is, is not) - 동일 메모리 테스트

x = [1,2,3]
y = [1,2,3]
### is
if x is y:
  print(x is y)
x = [1,2,3]
y = x
if x is y:
  print(x is y)

x = [1,2,3]
y = [1,2,3]
### is not
if x is not y:
  print(x is not y)
