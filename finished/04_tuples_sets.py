# 튜플은 순서는 있지만 변할 수 없는 데이터의 묶음이다.
# 동일한 값은 허용한다.

## 튜플 만들기
fruits = ('Apples', 'Oranges', 'Grapes')
fruits2 = tuple(('Apples', 'Oranges', 'Grapes'))

## 항목이 하나일 경우 끝에 쉼표 필요
fruits2 = ('Apples',)

## 튜플의 값 리턴하기
print(fruits[1])

## 튜플은 변경을 허용하지 않는다
fruits[0] = 'Pears'

## 튜플 전체 삭제는 가능
del fruits2

## 튜플의 항목 갯수
print(len(fruits))


# 셋은 순서가 없고 변경 가능하며 동일한 값이 허용되지 않는다.

## 셋 만들기
fruits_set = {'Apples', 'Oranges', 'Mango'}

## 셋에 항목이 있는지 검사하기
print('Apples' in fruits_set)

## 셋에 항목 추가하기
fruits_set.add('Grape')

## 셋의 항목 삭제하기
fruits_set.remove('Grape')

## 동일 내용을 넣을 경우?
fruits_set.add('Apples')

## 셋의 항목 지우기
fruits_set.clear()

## 셋 삭제
del fruits_set

print(fruits_set)
