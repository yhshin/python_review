# 파이썬에서 파일 다루는 방법

## 파일 열기
myFile = open('myfile.txt', 'w')

## 파일에 대한 정보 얻기
print('Name: ', myFile.name)
print('Is Closed : ', myFile.closed)
print('Opening Mode: ', myFile.mode)

## 파일에 쓰기
myFile.write('I love Python')
myFile.write(' and JavaScript')
myFile.close()

## 파일에 이어쓰기
myFile = open('myfile.txt', 'a')
myFile.write(' I also like PHP')
myFile.close()

## 파일로부터 읽기
myFile = open('myfile.txt', 'r+')
text = myFile.read(10)
print(text)
