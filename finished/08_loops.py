# for loop (list, tuple, dictionary, set, string 이용)
people = ['John', 'Paul', 'Sara', 'Susan']

## Simple for loop
for person in people:
  print(f'Current Person: {person}')

## Break
for person in people:
  if person == 'Sara':
    break
  print(f'Current Person: {person}')

## Continue
for person in people:
  if person == 'Sara':
    continue
  print(f'Current Person: {person}')

## range
for i in range(len(people)):
  print(f'Current Person: {people[i]}')

# While loops (특정 condition이 참인 동안)
count = 0
while count < 10:
  print(f'Count: {count}')
  count += 1
